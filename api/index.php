<?php

require_once("../config.php");
require_once("../includes/model.class.php");
require_once("../includes/user.class.php");
require_once("../includes/question.class.php");
require_once("../includes/answer.class.php");

header("Content-Type: application/json");

class API {

	public $db = null;
	public $user = null;
	public $question_api = null;
	public $answer_api = null;
	public $params = null;

	function __construct($db, $params){
		$this->db = $db;
		$this->user = new User($this->db);
		$this->question_api = new Question($this->db);
		$this->answer_api = new Answer($this->db);
		$this->params = $params;

		if (!isset($params['method'])){
			$this->raiseError("No method provided");
		}

		if ($this->params['method'] == "register"){
			$this->register();
		} elseif ($this->params['method'] == "login"){
			$this->login();
		} elseif ($this->params['method'] == "question"){
			$this->question();
		} elseif ($this->params['method'] == "answer"){
			$this->answer();
		} elseif ($this->params['method'] == "facebookLogin"){
			$this->facebookLogin();
		} elseif ($this->params['method'] == "stub"){
			$this->stub();
		} else {
			$this->raiseError("unknown method: {$this->params['method']}");
		}
	}

	public function getUserQuestions($user_id){
		$questions = array();
		$res = $this->answer_api->get(array("user_id" => $user_id), 0);
		foreach($res as $row_id => $row){
			$questions[] = $row['question_id'];
		}

		return $questions;
	}

	public function answer(){
		$errors = $this->answer_api->validate($this->params);
		if (!empty($errors)){
			$this->raiseError(json_encode($errors));
		}

		$this->answer_api->add($this->params);

	}

	public function question(){

		if (!isset($this->params['user_id']) || !$this->params['user_id']){
			$this->raiseError("missing param: user_id");
		}

		$question_ids = $this->getUserQuestions($this->params['user_id']);
		if (empty($question_ids)){
			$query = "SELECT * FROM questions ORDER BY rand() LIMIT 1";
		} else {
			$query = "SELECT * FROM questions WHERE id NOT IN (".implode(",", $question_ids).") ORDER BY rand() LIMIT 1";
		}

		$res = $this->db->query($query) or die($this->db->error." on line #".__LINE__);
		if ($res->num_rows){
			$row = $res->fetch_assoc();

			$ret = array();
			$ret['id'] = (int) $row['id'];
			$ret['question'] = $row['question'];
			$ret['category'] = $row['category'];
			$ret['correct'] = (int) $row['correct_answer'];
			$ret['current_post'] = 100000000; // TODO
			$ret['answers'] = array(
				$row['answer1'],
				$row['answer2'],
				$row['answer3'],
				$row['answer4']
			);

			print(json_encode($ret));
		}
	}

	public function register(){
		$errors = $this->user->validate($this->params);
		if (empty($errors)){
			$user_id = json_encode($this->user->add($this->params));

			print(json_encode(array("errors" => array(), "user_id" => $user_id)));
		} else {
			print(json_encode(array("errors" => $errors)));
		}
	}

	public function stub(){
		$data = array( 
		        "question" =>  "Mi az: 2 fule van megsem szatyor?",
		        "answers" => array(
		                "Szatyor",
		                "Nyul bazdmeg",
		                "Apad",
		                "Makosbukta"
		        ),
		        "correct" => 1,
		        "current_pos" => 1000000000
		);

		print(json_encode($data));
	}

	public function facebookLogin(){
		$errors = array();
		if (!isset($this->params['facebook_id']) || !$this->params['facebook_id']){
			$errors['facebook_id'] = "Missing Facebook ID";
		} 

		if (empty($errors)){
			$res = $this->user->get(array("facebook_id" => $this->params['facebook_id']), 0, 1);
			if (empty($res)){
				print(json_encode(array("errors" => array("facebook_id" => "User does not exist"))));	
			} else {
				print(json_encode(array("errors" => array(), "user_id" => $res['id'])));	
			}
		} else {
			print(json_encode(array("errors" => $errors)));
		}		

	}

	public function login(){
		$errors = array();
		if (!isset($this->params['username']) || !$this->params['username']){
			$errors['username'] = "Please enter your username";
		}

		if (!isset($this->params['password']) || !$this->params['password']){
			$errors['password'] = "Please enter your password";
		}

		if (empty($errors)){
			$password_hash = $this->user->hashPassword($this->params['password']);
			if (substr_count($this->params['username'], "@")){
				$check = array("email" => $this->params['username'], "password" => $password_hash);
			} else {
				$check = array("username" => $this->params['username'], "password" => $password_hash);
			}

			$res = $this->user->get($check, 0, 1);
			if (empty($res)){
				print(json_encode(array("errors" => array("username" => "Invalid login details"))));	
			} else {
				print(json_encode(array("errors" => array(), "user_id" => $res['id'])));	
			}
		} else {
			print(json_encode(array("errors" => $errors)));
		}
	}

	public function raiseError($message){
		print(json_encode(array("errors" => $message)));
		exit();
	}

}

$params = array();

if (isset($_GET) && !empty($_GET)){
	$params = $_GET;
} elseif (isset($_POST) && !empty($_POST)){
	$params = $_POST;
}

$db = mysqlConnect($dbhost, $dbuser, $dbpass, $dbname);
$api = new API($db, $params);