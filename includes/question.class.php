<?php

class Question extends Model {

    public $db = null;
    public $schema = array( "id"    =>  array("type" => "int",
                                              "unique" => true,
                                              "notnull" => true,
                                              "nice_name" => "Id"),

                            "question" =>  array( "type" => "string",
                                                  "unique" => true,
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Question"),
                            "answer1" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Answer1"),
                            "answer2" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Answer2"),
                            "answer3" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Answer3"),
                            "answer4" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Answer4"),
                            "correct_answer" =>  array(  "type" => "int",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Correct answer"),
                            "category" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => false,
                                                  "nice_name" => "Category"),
                            );
    public $table = "questions";
    public $nice_name = "Question";

    function __construct($db){
        global $baseurl, $basepath;

        $this->db = $db;
    }

}
