<?php

class Answer extends Model {

    public $db = null;
    public $schema = array( "id"    =>  array("type" => "int",
                                              "unique" => true,
                                              "notnull" => true,
                                              "nice_name" => "Id"),

                            "question_id" =>  array( "type" => "int",
                                                  "unique" => false,
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "QuestionId"),
                            "user_id" =>  array( "type" => "int",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "UserId"),
                            "answer" =>  array( "type" => "int",
                                                  "required" => true,
                                                  "nice_name" => "Answer"),
                            "correct" =>  array( "type" => "int",
                                                  "required" => true,
                                                  "nice_name" => "Correct")
                            );
    public $table = "user_answers";
    public $nice_name = "Answer";

    function __construct($db){
        global $baseurl, $basepath;

        $this->db = $db;
    }

}
