<?php

class User extends Model {

    public $db = null;
    public $schema = array( "id"    =>  array("type" => "int",
                                              "unique" => true,
                                              "notnull" => true,
                                              "nice_name" => "Id"),

                            "username" =>  array( "type" => "string",
                                                  "unique" => true,
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Username"),
                            "password" =>  array( "type" => "string",
                                                  "required" => true,
                                                  "notnull" => true,
                                                  "nice_name" => "Password"),
                            "email" =>  array(  "type" => "string",
                                                "unique" => true,
                                                "required" => true,
                                                "notnull" => true,
                                                "nice_name" => "Email address"),
                            "facebook_id" =>  array("type" => "string",
                                                    "unique" => true,
                                                    "nice_name" => "Facebook ID")
                            );
    public $table = "users";
    public $nice_name = "User";

    function __construct($db){
        global $baseurl, $basepath;

        $this->db = $db;
        $this->schema['thumbnail']['path'] = $basepath."/thumbs";
        $this->schema['thumbnail']['url_prefix'] = $baseurl."/thumbs";
    }

    public function validateEmail($email){
        $isValid = true;
        $atIndex = strrpos($email, "@");
        if (is_bool($atIndex) && !$atIndex){
            $isValid = false;
        }  else {
            $domain = substr($email, $atIndex+1);
            $local = substr($email, 0, $atIndex);
            $localLen = strlen($local);
            $domainLen = strlen($domain);
            if ($localLen < 1 || $localLen > 64){
                // local part length exceeded
                $isValid = false;
            } else if ($domainLen < 1 || $domainLen > 255){
                // domain part length exceeded
                $isValid = false;
            } else if ($local[0] == '.' || $local[$localLen-1] == '.'){
                // local part starts or ends with '.'
                $isValid = false;
            } else if (preg_match('/\\.\\./', $local)){
                // local part has two consecutive dots
                $isValid = false;
            } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)){
                // character not valid in domain part
                $isValid = false;
            } else if (preg_match('/\\.\\./', $domain)){
                // domain part has two consecutive dots
                $isValid = false;
            } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',str_replace("\\\\","",$local))){
                // character not valid in local part unless 
                // local part is quoted
                if (!preg_match('/^"(\\\\"|[^"])+"$/',str_replace("\\\\","",$local))){
                    $isValid = false;
                }
            }
        }
        return $isValid;
    }

    public function validate($params, $update_id = false){
        $errors = parent::validate($params, $update_id);

        if (!isset($errors['email']) && !$this->validateEmail($params['email'])){
            $errors['email'] = "Invalid email address provided";
        }

        if (!isset($errors['username'])){
            if (strlen($params['username']) < 5 || strlen($params['username']) > 30){
                $errors['username'] = "Username must be between 5 and 30 characters";
            } elseif (preg_match("/[^a-zA-Z0-9_]/", $params['username'])){
                $errors['username'] = "Username can only contain alphanumeric characters";
            }
        }

        if (!isset($errors['password'])){
            if (strlen($params['password']) < 5){
                $errors['password'] = "Password must be at least 5 characters long";
            }
        }

        return $errors;
    }

    public function hashPassword($password){
        $iterations = 10;
        $salt = md5($password).";billion$";
        $hash = crypt($password, $salt);
        for ($i = 0; $i < 10; ++$i){
            $hash = crypt($hash.$password, $salt);
        }

        return $hash;
    }

    public function add($params){
        $params['password'] = $this->hashPassword($params['password']);

        return parent::add($params);
    }

}