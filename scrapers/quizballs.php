<?php

set_time_limit(0);
ini_set("memory_limit", "1024M");

if (!isset($argv[1])){
	print("usage: php {$argv[0]} [URL]\n");
	exit();
}