<?php

// http://www.pubquizarea.com/multiple-choice/general-knowledge-quiz/start/&next=30/32/1/

set_time_limit(0);
ini_set("memory_limit", "1024M");

if (!isset($argv[1])){
    print("usage: php {$argv[0]} [URL]\n");
    exit();
}

require_once("../includes/curl.class.php");
require_once("../config.php");
require_once("../includes/model.class.php");
require_once("../includes/question.class.php");

$curl = new Curl();
$db = mysqlConnect($dbhost, $dbuser, $dbpass, $dbname);

$question = new Question($db);

$url = $argv[1];

while(true){
    print("getting main page: {$url}\n");

    $page = $curl->get($url);

    libxml_use_internal_errors(true);

    $dom = new DOMDocument();
    $dom->loadHTML($page);

    $links = $dom->getElementsByTagName('a');
    $next_url = null;

    $quizzes = array();
    for($i=0; $i<$links->length; $i++){
        $href = $links->item($i)->getAttribute("href");

        if (trim(strtolower($links->item($i)->textContent)) == "next" && substr_count($href, "&next=")){
            if ($href[0] == "/"){
                $next_url = "http://www.pubquizarea.com/".$href;
            } else {
                $next_url = "http://www.pubquizarea.com".$href;
            }
            
        }

        if (strpos($href, "view_question_and_answer_quizzes.php") !== 0){
            continue;
        }

        $quizzes[] = $href;
    }

    if (count($quizzes)){
        foreach($quizzes as $quiz_url){
            print($quiz_url."...\n");
            $page = $curl->get("http://www.pubquizarea.com/".$quiz_url);

            $dom = new DOMDocument();
            $dom->loadHTML($page);

            $h2s = $dom->getElementsByTagName('h2');
            if (!$h2s->length){
                continue;
            }

            $category = trim($h2s->item(0)->textContent);
            $category = preg_replace("/[^a-zA-Z0-9]+$/", "", $category);
            $category = preg_replace("/\s?\d+$/", "", $category);

            print("category: {$category}\n");

            $divs = $dom->getElementsByTagName('div');
            for($i=0; $i<$divs->length; $i++){
                $id = $divs->item($i)->getAttribute("id");

                if (strpos($id, "q_and_a_") !== 0){
                    continue;
                }

                $subdivs = $divs->item($i)->getElementsByTagName('div');

                $question_data = array();
                $question_data['answers'] = array();
                for($j=0; $j<$subdivs->length; $j++){
                    if ($subdivs->item($j)->getAttribute("class") == "left green"){
                        $question_data['question'] = preg_replace("/^\d+\.\ ?/", "", $subdivs->item($j)->textContent);
                    }

                    if ($subdivs->item($j)->getAttribute("class") == "written_quiz_answer"){
                        $question_data['answers'][] = trim($subdivs->item($j)->textContent);

                        $spans = $subdivs->item($j)->getElementsByTagName('span');
                        if ($spans->length){
                            for($k=0; $k<$spans->length; $k++){
                                if ($spans->item($k)->getAttribute("class") == "blue"){
                                    $question_data['correct_answer'] = count($question_data['answers']) - 1;
                                }
                            }
                        }
                    }
                }

                if (count($question_data['answers']) != 4 || !isset($question_data['correct_answer'])){
                    continue;
                }

                $db_data = array();
                $db_data['question'] = $question_data['question'];
                $db_data['answer1'] = $question_data['answers'][0];
                $db_data['answer2'] = $question_data['answers'][1];
                $db_data['answer3'] = $question_data['answers'][2];
                $db_data['answer4'] = $question_data['answers'][3];
                $db_data['correct_answer'] = $question_data['correct_answer'];
                $db_data['category'] = $category;
                
                $errors = $question->validate($db_data);
                if (!count($errors)){
                    $question->add($db_data);
                    print("added: {$db_data['question']}\n");
                }
            }
        }
    }

    // next page
    if (!$next_url){
        break;
    }

    $url = $next_url;

}